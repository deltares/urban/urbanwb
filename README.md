# Urban water balance model

The `urbanwb` model is a lumped conceptual multi-reservoir hydrological model that quickly
simulates dominant hydrological processes of an urban water system. 

The main documentation can be found at
https://publicwiki.deltares.nl/display/AST/Urban+Water+balance+model.

This model was proposed by Toine Vergroesen (Deltares) in 2012. Wenxing Zhang converted the
original Excel version into this Python module.

To install the package, please do "pip install -e ." with cmd in the package root directory.

The model is available under the MIT license.

For issues regarding the calculation formulas, please contact toine.vergroesen@deltares.nl.
